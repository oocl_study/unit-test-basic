package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "project");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project);

        // then
        Assertions.assertSame(ExpenseType.INTERNAL_PROJECT_EXPENSE, expenseType);

    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project project_a = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project_a);

        // then
        Assertions.assertSame(ExpenseType.EXPENSE_TYPE_A, expenseType);

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project project_b = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project_b);

        // then
        Assertions.assertSame(ExpenseType.EXPENSE_TYPE_B, expenseType);

    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project project_c = new Project(ProjectType.EXTERNAL, "Project C");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project_c);

        // then
        Assertions.assertSame(ExpenseType.OTHER_EXPENSE, expenseType);

    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project project_d = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "Project D");
        ExpenseService expenseService = new ExpenseService();

        // when

        // then
        Assertions.assertThrows(UnexpectedProjectTypeException.class, () -> expenseService.getExpenseCodeByProject(project_d), "xx");


    }
}